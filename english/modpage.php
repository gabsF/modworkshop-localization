<?php

$l['mydownloads_being_updated'] = 'Updating';
$l['mydownloads_meta_by'] = '{1} by {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = 'You cannot rate your own mods.';
$l['mydownloads_collaborators_desc'] = 'Collaborators are people who you give permission to edit the mod. They cannot delete the mod, transfer ownership of it, or edit collaborators';
$l['mydownloads_comment_banned'] = "You cannot comment while you are banned.";
$l['mydownloads_delete_confirm'] = "Are you sure you want to delete this comment?";
$l['mydownloads_download_description'] = "Description";
$l['mydownloads_download_is_suspended'] = '
This mod has been suspended and is only visible to its author and the site staff.
<br/>The suspension is either temporarily for investigation or permanently due to violating the <a style="text-decoration:underline;" href="/rules">rules</a>.
<br/>Should you wish to contact the staff about it or if your mod has been updated to follow the rules you can send an "Unsuspend Application" <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">here</a>.';
$l['mydownloads_download_changelog'] = 'Changelog';
$l['mydownloads_download'] = "Download";
$l['mydownloads_license'] = 'License';
$l['mydownloads_report_download'] = 'Report Mod';
$l['show_download_link_warn'] = 'Be careful of suspicious links. If you think the link is malicious, please report the mod';
$l['show_download_link'] = 'Show Download Link';
$l['show_files'] = 'Show Files';
$l['submitted_by'] = 'Submitted By';
$l['subscribe_commnet_help'] = 'Get notified when someone comments in this comment section';
$l['follow_mod_help'] = 'Follow this mod to show it in your followed mods and get notified when the mod gets updated';
$l['mydownloads_download_comments'] = "Comments";
$l['mydownloads_unsuspend_it'] = 'Unsuspend';
$l['mydownloads_suspend_it'] = 'Suspend';
$l['mydownloads_files_alert'] = '<strong>NO FILES</strong> | Since the mod has no files, the mod is invisible.';
$l['mydownloads_files_alert_waiting'] = '<strong>NO APPROVED FILES</strong> | This mod is currently invisible until the files you uploaded are approved.';
