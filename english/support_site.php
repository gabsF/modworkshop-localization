<?php

$l['already_support'] = "You already support us. Thank you! ❤"; 
$l['support_mws_details'] = "
<h4>Hate ads? Want to support us?</h4>

No advertisements.<br>
A role in Discord and the site.<br>
A badge showing your support.<br>
Custom color for your name in the site and Discord server*<br>
";
$l['support_mws_extra_details'] = "
*In discord, you will need to ask a moderator for the custom color role.<br>
*To get a refund or extend the plan please talk with an admin.<br>
";
$l['support_mws_not_logged'] = 'In order to support us, you must first be logged in. <a href="/login">Sign in through Steam</a>';

